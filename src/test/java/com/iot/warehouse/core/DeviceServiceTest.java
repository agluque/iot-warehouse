package com.iot.warehouse.core;

import com.iot.warehouse.ApplicationConfig;
import com.iot.warehouse.DeviceFixtures;
import com.iot.warehouse.SimCardFixtures;
import com.iot.warehouse.core.exception.DeviceNotFoundException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.ArgumentCaptor;
import org.mockito.Captor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.util.List;
import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(MockitoExtension.class)
class DeviceServiceTest {

    @InjectMocks
    private DeviceService deviceService;

    @Mock
    private ApplicationConfig applicationConfig;

    @Mock
    private DeviceRepository deviceRepository;

    @Nested
    class FindDevices {

        @Test
        void shouldFindDevicesBySimCardStatus() {
            var simCardStatus = SimCardStatus.ACTIVE;
            var expectedDevices = List.of(
                    DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard())
            );
            var pageable = Pageable.ofSize(10).withPage(0);

            when(deviceRepository.findAllBySimCardStatus(simCardStatus, pageable))
                    .thenReturn(new PageImpl<>(expectedDevices));

            var actualDevices = deviceService.findDevicesBySimCardStatus(simCardStatus, pageable);

            assertThat(actualDevices).containsAll(expectedDevices);
            verify(deviceRepository, times(1)).findAllBySimCardStatus(simCardStatus, pageable);
        }

        @Test
        void shouldFindAllDevices() {
            var expectedDevices = List.of(
                    DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard())
            );
            var page = Pageable.ofSize(10).withPage(0);

            when(deviceRepository.findAll(page)).thenReturn(new PageImpl<>(expectedDevices));

            var actualDevices = deviceService.findAllDevices(page);

            assertThat(actualDevices).containsAll(expectedDevices);
            verify(deviceRepository, times(1)).findAll(page);
        }

        @Test
        void shouldFindOnSaleDevices() {
            var minTemperature = -25;
            var maxTemperature = 85;
            var expectedDevices = List.of(
                    DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard())
            );
            var pageable = Pageable.ofSize(20).withPage(0);

            when(deviceRepository.findAllByStatusAndTemperatureBetween(DeviceStatus.READY, -25,
                    85, pageable)).thenReturn(new PageImpl<>(expectedDevices));
            when(applicationConfig.getMinTemperature()).thenReturn(minTemperature);
            when(applicationConfig.getMaxTemperature()).thenReturn(maxTemperature);

            var actualDevices = deviceService.findOnSaleDevices(pageable);

            assertThat(actualDevices.getContent()).containsAll(expectedDevices);

            verify(deviceRepository, times(1))
                    .findAllByStatusAndTemperatureBetween(DeviceStatus.READY,
                            minTemperature, maxTemperature, pageable);
        }

    }

    @Nested
    class DeleteDevices {

        @Test
        void shouldRemoveDeviceById() {
            var deviceId = 1L;

            when(deviceRepository.deleteDeviceById(deviceId)).thenReturn(1);

            deviceService.removeDeviceById(deviceId);

            verify(deviceRepository, times(1)).deleteDeviceById(deviceId);
        }

        @Test
        void whenRemovingDeviceShouldRaiseDeviceNotFoundExceptionIfDeviceDoesNotExist() {
            var deviceId = 1L;

            when(deviceRepository.deleteDeviceById(deviceId)).thenReturn(0);

            assertThrows(DeviceNotFoundException.class, () -> deviceService.removeDeviceById(deviceId));
        }

    }

    @Nested
    class UpdateDeviceConfigurationStatus {

        @Captor
        private ArgumentCaptor<Device> deviceArgumentCaptor;

        @Test
        void shouldUpdateDeviceConfigurationStatus() {
            var notReadyDevice = DeviceFixtures.notReadyDevice(SimCardFixtures.activeSimCard());
            var expected = notReadyDevice.toBuilder().status(DeviceStatus.READY).build();

            when(deviceRepository.findById(notReadyDevice.getId())).thenReturn(Optional.of(notReadyDevice));
            when(deviceRepository.save(expected)).thenReturn(expected);

            var device = deviceService.updateDeviceConfigurationStatus(notReadyDevice.getId(), DeviceStatus.READY);

            verify(deviceRepository).save(deviceArgumentCaptor.capture());

            assertThat(device).usingRecursiveComparison().isEqualTo(expected);
            assertThat(deviceArgumentCaptor.getValue().getStatus()).isEqualTo(DeviceStatus.READY);

            verify(deviceRepository, times(1)).findById(notReadyDevice.getId());
        }

        @Test
        void shouldRaiseDeviceNotFoundExceptionIfDeviceDoesNotExistingWhenUpdatingStatus() {
            var nonExistingDeviceId = 1L;

            when(deviceRepository.findById(nonExistingDeviceId)).thenReturn(Optional.empty());

            assertThrows(DeviceNotFoundException.class,
                    () -> deviceService.updateDeviceConfigurationStatus(nonExistingDeviceId, DeviceStatus.READY));

            verifyNoMoreInteractions(deviceRepository);
        }
    }

}
