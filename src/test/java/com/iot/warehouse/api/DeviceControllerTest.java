package com.iot.warehouse.api;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iot.warehouse.DeviceFixtures;
import com.iot.warehouse.SimCardFixtures;
import com.iot.warehouse.api.model.UpdateDeviceConfigurationRequest;
import com.iot.warehouse.core.DeviceService;
import com.iot.warehouse.core.DeviceStatus;
import com.iot.warehouse.core.SimCardStatus;
import com.iot.warehouse.core.exception.DeviceNotFoundException;
import org.junit.jupiter.api.Nested;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.params.ParameterizedTest;
import org.junit.jupiter.params.provider.MethodSource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import java.util.List;

import static org.mockito.Mockito.*;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@WebMvcTest(controllers = DeviceController.class)
class DeviceControllerTest {

    private static final String DEVICE_API_BASE_PATH = "/api/warehouse/devices";

    @Autowired
    private MockMvc mockMvc;

    @MockBean
    private DeviceService deviceService;

    @Autowired
    private ObjectMapper objectMapper;

    @Nested
    class FindDevices {

        @MethodSource("com.iot.warehouse.api.DeviceControllerTest#buildValidSimCardStatusParameters")
        @ParameterizedTest
        void shouldRetrieveDevicesBySimCardStatusWhenSimCardStatusParameterIsValid(String simCardStatus) throws Exception {
            var pageable = Pageable.ofSize(5).withPage(1);

            when(deviceService.findDevicesBySimCardStatus(SimCardStatus.valueOf(simCardStatus), pageable)).thenReturn(Page.empty());

            mockMvc.perform(get(DEVICE_API_BASE_PATH)
                    .param("simCardStatus", simCardStatus)
                    .param("page", String.valueOf(pageable.getPageNumber()))
                    .param("size", String.valueOf(pageable.getPageSize()))
                    .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isOk()).andReturn();

            verify(deviceService, times(1))
                    .findDevicesBySimCardStatus(SimCardStatus.valueOf(simCardStatus), pageable);
        }

        @Test
        void shouldRetrieveAllDevicesWhenSimCardStatusParameterIsMissing() throws Exception {
            when(deviceService.findAllDevices(any(Pageable.class))).thenReturn(Page.empty());

            mockMvc.perform(get(DEVICE_API_BASE_PATH)
                    .contentType(MediaType.APPLICATION_JSON)
            ).andExpect(status().isOk()).andReturn();

            verify(deviceService, times(1)).findAllDevices(any(Pageable.class));
        }

        @Test
        void shouldReturnBadRequestResponseWhenSimCardStatusParameterIsNotValid() throws Exception {
            var invalidSimCardStatus = "invalid";

            mockMvc.perform(get(DEVICE_API_BASE_PATH)
                    .param("simCardStatus", invalidSimCardStatus)
                    .contentType(MediaType.APPLICATION_JSON))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.statusCode").value(400))
                    .andExpect(jsonPath("$.message").value(String.format("Invalid simCardStatus value: %s", invalidSimCardStatus)))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON));

            verifyNoInteractions(deviceService);
        }

    }

    @Nested
    class DeleteDevice {

        @Test
        void shouldReturnNotFoundResponseWhenDeviceDoesNotExist() throws Exception {
            var nonExistingDeviceId = 1L;

            doThrow(new DeviceNotFoundException(nonExistingDeviceId)).when(deviceService).removeDeviceById(nonExistingDeviceId);

            mockMvc.perform(delete("/api/warehouse/devices/{id}", nonExistingDeviceId))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$.statusCode").value(404))
                    .andExpect(jsonPath("$.message").value(String.format("Device with ID %s not found", nonExistingDeviceId)))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            ;
        }

        @Test
        void shouldReturnBadRequestResponseIfDeviceIdIsNotValid() throws Exception {
            var notValidDeviceId = "notValidId";

            mockMvc.perform(delete("/api/warehouse/devices/{id}", "notValidId"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.statusCode").value(400))
                    .andExpect(jsonPath("$.message").value(String.format("Invalid id value: %s", notValidDeviceId)))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            ;
        }

    }

    @Nested
    class UpdateDeviceConfigurationStatus {

        @Test
        void shouldUpdateDeviceConfigurationStatus() throws Exception {
            var existingDevice = DeviceFixtures.notReadyDevice(SimCardFixtures.waitingForActivationSimCard());
            var updateDevice = existingDevice.toBuilder().status(DeviceStatus.READY).build();
            var request = new UpdateDeviceConfigurationRequest(DeviceStatus.READY);

            when(deviceService.updateDeviceConfigurationStatus(existingDevice.getId(), DeviceStatus.READY)).thenReturn(updateDevice);

            mockMvc.perform(patch("/api/warehouse/devices/{id}", existingDevice.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(request)))
                    .andExpect(status().isOk())
                    .andExpect(jsonPath("$.id").value(existingDevice.getId()))
                    .andExpect(jsonPath("$.temperature").value(existingDevice.getTemperature()))
                    .andExpect(jsonPath("$.name").value(existingDevice.getName()))
                    .andExpect(jsonPath("$.status").value(DeviceStatus.READY.name()))
                    .andExpect(jsonPath("$.simCard.status").value(existingDevice.getSimCard().getStatus().name()))
                    .andExpect(jsonPath("$.simCard.id").value(existingDevice.getSimCard().getId().toString()))
                    .andExpect(jsonPath("$.simCard.operatorCode").value(existingDevice.getSimCard().getOperatorCode()))
                    .andExpect(jsonPath("$.simCard.country").value(existingDevice.getSimCard().getCountry()))
            ;

            verify(deviceService, times(1)).updateDeviceConfigurationStatus(existingDevice.getId(), DeviceStatus.READY);
        }

        @Test
        void shouldReturnBadRequestResponseIfDeviceStatusIsMissingInTheRequest() throws Exception {
            var existingDevice = DeviceFixtures.notReadyDevice(SimCardFixtures.waitingForActivationSimCard());

            mockMvc.perform(patch("/api/warehouse/devices/{id}", existingDevice.getId())
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{}"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.statusCode").value(400))
                    .andExpect(jsonPath("$.message").value("deviceStatus: must not be null"))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            ;
        }

        @Test
        void shouldReturnBadRequestIfProvidedStatusIsInvalid() throws Exception {
            mockMvc.perform(patch("/api/warehouse/devices/{id}", 1L)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content("{" +
                            "   \"deviceStatus\":\"invalidDeviceStatusValue\"" +
                            "}"))
                    .andExpect(status().isBadRequest())
                    .andExpect(jsonPath("$.statusCode").value(400))
                    .andExpect(jsonPath("$.message").value("Malformed JSON request"))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            ;
        }

        @Test
        void shouldReturnNotFoundResponseIfDeviceDoesNotExist() throws Exception {
            var nonExistingDeviceId = 1L;
            var request = new UpdateDeviceConfigurationRequest(DeviceStatus.READY);

            when(deviceService.updateDeviceConfigurationStatus(nonExistingDeviceId, DeviceStatus.READY))
                    .thenThrow(new DeviceNotFoundException(nonExistingDeviceId));

            mockMvc.perform(patch("/api/warehouse/devices/{id}", nonExistingDeviceId)
                    .contentType(MediaType.APPLICATION_JSON)
                    .content(objectMapper.writeValueAsString(request)))
                    .andExpect(status().isNotFound())
                    .andExpect(jsonPath("$.statusCode").value(404))
                    .andExpect(jsonPath("$.message").value(String.format("Device with ID %s not found", nonExistingDeviceId)))
                    .andExpect(content().contentType(MediaType.APPLICATION_JSON))
            ;
        }

    }

    private static List<String> buildValidSimCardStatusParameters() {
        return List.of(
                "ACTIVE",
                "WAITING_FOR_ACTIVATION",
                "BLOCKED",
                "DEACTIVATED"
        );
    }

}