CREATE EXTENSION IF NOT EXISTS "uuid-ossp";

INSERT INTO sim_card (sim_card_id, operator_code, country, status)
  VALUES (uuid_generate_v4(), 'OC', 'Italy', 'ACTIVE');
INSERT INTO device (name, temperature, sim_card_id, status)
  VALUES ('DEVICE 1', 50, lastVal(), 'READY');

INSERT INTO sim_card (sim_card_id, operator_code, country, status)
  VALUES (uuid_generate_v4(), 'DC', 'Spain', 'WAITING_FOR_ACTIVATION');
INSERT INTO device (name, temperature, sim_card_id, status)
  VALUES ('DEVICE 2', 50, lastVal(), 'NOT_READY');

INSERT INTO sim_card (sim_card_id, operator_code, country, status)
  VALUES (uuid_generate_v4(), 'DC', 'Spain', 'WAITING_FOR_ACTIVATION');
INSERT INTO device (name, temperature, sim_card_id, status)
  VALUES ('DEVICE 3', 100, lastVal(), 'READY');

INSERT INTO sim_card (sim_card_id, operator_code, country, status)
  VALUES (uuid_generate_v4(), 'DC', 'Spain', 'BLOCKED');
INSERT INTO device (name, temperature, sim_card_id, status)
  VALUES ('DEVICE 4', 20, lastVal(), 'NOT_READY');

INSERT INTO sim_card (sim_card_id, operator_code, country, status)
  VALUES (uuid_generate_v4(), 'MV', 'France', 'DEACTIVATED');
INSERT INTO device (name, temperature, sim_card_id, status)
  VALUES ('DEVICE 5', 33, lastVal(), 'NOT_READY');