CREATE SEQUENCE hibernate_sequence INCREMENT 1 MINVALUE 1
    MAXVALUE 9223372036854775807 START 1
CACHE 1;

CREATE TABLE sim_card_status
(
    name VARCHAR(25) PRIMARY KEY
);

CREATE TABLE sim_card
(
    id            BIGSERIAL PRIMARY KEY,
    sim_card_id   uuid NOT NULL UNIQUE,
    operator_code VARCHAR(25) NOT NULL,
    country       VARCHAR(25) NOT NULL,
    status        VARCHAR(25) NOT NULL,

    CONSTRAINT sim_card_status_fk FOREIGN KEY (status) REFERENCES sim_card_status (name)
);

INSERT INTO sim_card_status
values ('ACTIVE');
INSERT INTO sim_card_status
values ('WAITING_FOR_ACTIVATION');
INSERT INTO sim_card_status
values ('BLOCKED');
INSERT INTO sim_card_status
values ('DEACTIVATED');

CREATE TABLE device_status
(
    name VARCHAR(25) PRIMARY KEY
);

CREATE TABLE device
(
    id          BIGSERIAL PRIMARY KEY,
    name        VARCHAR(25) NOT NULL,
    temperature SMALLINT    NOT NULL,
    sim_card_id BIGSERIAL   NOT NULL,
    status      VARCHAR(25) NOT NULL,

    CONSTRAINT sim_card_fk FOREIGN KEY (sim_card_id) REFERENCES sim_card (id),
    CONSTRAINT device_status_fk FOREIGN KEY (status) REFERENCES device_status (name)
);

INSERT INTO device_status values ('READY');
INSERT INTO device_status values ('NOT_READY');