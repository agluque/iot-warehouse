package com.iot.warehouse;

import lombok.Getter;
import lombok.Setter;
import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

@Configuration
@ConfigurationProperties(prefix = "application")
@Setter
@Getter
public class ApplicationConfig {

    private int minTemperature;
    private int maxTemperature;

}
