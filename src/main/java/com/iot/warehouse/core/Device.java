package com.iot.warehouse.core;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.*;
import javax.validation.constraints.NotNull;

@Builder(toBuilder = true)
@Data
@NoArgsConstructor
@AllArgsConstructor
@Entity
@Table(name = "device")
public class Device {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @NotNull
    private String name;

    @NotNull
    private Integer temperature;

    @NotNull
    @OneToOne(cascade = CascadeType.ALL)
    @JoinColumn(name = "simCardId", referencedColumnName = "id")
    private SimCard simCard;

    @NotNull
    @Enumerated(EnumType.STRING)
    private DeviceStatus status;

}
