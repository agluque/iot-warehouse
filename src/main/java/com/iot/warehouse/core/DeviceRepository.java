package com.iot.warehouse.core;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

@Repository
public interface DeviceRepository extends JpaRepository<Device, Long> {

    Page<Device> findAllBySimCardStatus(SimCardStatus simCardStatus, Pageable pageable);

    @Modifying
    @Query(value = "DELETE FROM Device WHERE id = :id", nativeQuery = true)
    int deleteDeviceById(Long id);

    Page<Device> findAllByStatusAndTemperatureBetween(DeviceStatus deviceStatus, int minTemperature,
                                                      int maxTemperature, Pageable pageable);

}
