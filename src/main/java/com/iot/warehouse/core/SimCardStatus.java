package com.iot.warehouse.core;

public enum SimCardStatus {
    ACTIVE,
    WAITING_FOR_ACTIVATION,
    BLOCKED,
    DEACTIVATED
}
