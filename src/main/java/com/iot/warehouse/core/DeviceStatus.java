package com.iot.warehouse.core;

public enum DeviceStatus {
    READY,
    NOT_READY
}
