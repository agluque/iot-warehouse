package com.iot.warehouse.core;

import com.iot.warehouse.ApplicationConfig;
import com.iot.warehouse.core.exception.DeviceNotFoundException;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

@Service
@RequiredArgsConstructor
@Slf4j
public class DeviceService {

    private final ApplicationConfig applicationConfig;
    private final DeviceRepository deviceRepository;

    @Transactional(readOnly = true)
    public Page<Device> findDevicesBySimCardStatus(SimCardStatus simCardStatus, Pageable pageable) {
        log.debug("Finding devices with sim card status: {}, pageable: {}", simCardStatus, pageable);
        return deviceRepository.findAllBySimCardStatus(simCardStatus, pageable);
    }

    @Transactional(readOnly = true)
    public Page<Device> findAllDevices(Pageable pageable) {
        log.debug("Finding all devices");
        return deviceRepository.findAll(pageable);
    }

    @Transactional
    public void removeDeviceById(Long deviceId) {
        log.debug("Removing device with ID: {}", deviceId);
        int modifiedRows = deviceRepository.deleteDeviceById(deviceId);

        if (modifiedRows == 0) {
            throw new DeviceNotFoundException(deviceId);
        }
    }

    @Transactional
    public Device updateDeviceConfigurationStatus(Long deviceId, DeviceStatus deviceStatus) {
        log.debug("Updating device configuration status: {} for device with ID: {}", deviceStatus, deviceId);
        Device device = deviceRepository.findById(deviceId)
                .orElseThrow(() -> new DeviceNotFoundException(deviceId));

        device.setStatus(deviceStatus);

        return deviceRepository.save(device);
    }

    @Transactional(readOnly = true)
    public Page<Device> findOnSaleDevices(Pageable pageable) {
        log.debug("Finding on sale devices: {}", pageable);
        return deviceRepository.findAllByStatusAndTemperatureBetween(DeviceStatus.READY,
                applicationConfig.getMinTemperature(), applicationConfig.getMaxTemperature(), pageable);
    }

}
