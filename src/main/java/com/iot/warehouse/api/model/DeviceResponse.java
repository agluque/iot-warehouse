package com.iot.warehouse.api.model;

import com.iot.warehouse.core.Device;
import lombok.Getter;
import org.springframework.data.domain.Page;

import java.util.List;

@Getter
public class DeviceResponse {

    private final List<Device> devices;

    private final int page;

    private final int totalPages;

    public DeviceResponse(Page<Device> devicePage) {
        this.devices = devicePage.getContent();
        this.page = devicePage.getNumber();
        this.totalPages = devicePage.getTotalPages();
    }

}
