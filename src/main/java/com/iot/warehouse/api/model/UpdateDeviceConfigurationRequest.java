package com.iot.warehouse.api.model;

import com.iot.warehouse.core.DeviceStatus;
import lombok.AccessLevel;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.NotNull;

@Data
@AllArgsConstructor
@NoArgsConstructor(access = AccessLevel.PRIVATE)
public class UpdateDeviceConfigurationRequest {

    @NotNull
    private DeviceStatus deviceStatus;

}
