package com.iot.warehouse.api.model;

import lombok.Getter;
import lombok.RequiredArgsConstructor;

@RequiredArgsConstructor
@Getter
public class ApiErrorResponse {

    private final int statusCode;

    private final String message;

}
