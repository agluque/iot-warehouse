package com.iot.warehouse.api;

import com.iot.warehouse.api.model.DeviceResponse;
import com.iot.warehouse.api.model.UpdateDeviceConfigurationRequest;
import com.iot.warehouse.core.Device;
import com.iot.warehouse.core.DeviceService;
import com.iot.warehouse.core.SimCardStatus;
import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import lombok.RequiredArgsConstructor;
import org.springdoc.core.converters.models.PageableAsQueryParam;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/warehouse/devices")
@RequiredArgsConstructor
public class DeviceController {

    private final DeviceService deviceService;

    @PageableAsQueryParam
    @Operation(summary = "Get a list of devices")
    @GetMapping(produces = "application/json")
    public DeviceResponse getDevicesBySimCardStatus(
            @Parameter(description = "filter devices by SIM Card status") @RequestParam(name = "simCardStatus", required = false)
                    SimCardStatus simCardStatus, @Parameter(hidden = true) Pageable pageable) {
        return simCardStatus == null ? new DeviceResponse(deviceService.findAllDevices(pageable)) :
                new DeviceResponse(deviceService.findDevicesBySimCardStatus(simCardStatus, pageable));
    }

    @PageableAsQueryParam
    @Operation(summary = "Get a list of devices on sale")
    @GetMapping(value = "/on-sale", produces = "application/json")
    public DeviceResponse getOnSaleDevices(@Parameter(hidden = true) Pageable pageable) {
        return new DeviceResponse(deviceService.findOnSaleDevices(pageable));
    }

    @Operation(summary = "Delete a device by device ID")
    @DeleteMapping(value = "/{id}")
    public void deleteDeviceById(@Parameter(description = "Device ID") @PathVariable Long id) {
        deviceService.removeDeviceById(id);
    }

    @Operation(summary = "Update device configuration status")
    @PatchMapping(value = "/{id}", consumes = "application/json", produces = "application/json")
    public Device updateDeviceConfigurationStatus(
            @Parameter(description = "Device ID") @PathVariable Long id,
            @Valid @RequestBody UpdateDeviceConfigurationRequest request
    ) {
        return deviceService.updateDeviceConfigurationStatus(id, request.getDeviceStatus());
    }

}
