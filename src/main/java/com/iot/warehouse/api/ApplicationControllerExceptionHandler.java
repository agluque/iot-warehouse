package com.iot.warehouse.api;

import com.iot.warehouse.api.model.ApiErrorResponse;
import com.iot.warehouse.core.exception.DeviceNotFoundException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

@RestControllerAdvice
public class ApplicationControllerExceptionHandler extends ResponseEntityExceptionHandler {

    @ExceptionHandler(MethodArgumentTypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    public ApiErrorResponse handleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex) {
        return new ApiErrorResponse(HttpStatus.BAD_REQUEST.value(), String.format("Invalid %s value: %s", ex.getName(), ex.getValue()));
    }

    @ExceptionHandler(DeviceNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    public ApiErrorResponse handleDeviceNotFoundException(DeviceNotFoundException ex) {
        return new ApiErrorResponse(HttpStatus.NOT_FOUND.value(), ex.getMessage());
    }

    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        FieldError fieldError = ex.getBindingResult().getFieldErrors().get(0);
        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(status.value(),
                String.format("%s: %s", fieldError.getField(), fieldError.getDefaultMessage()));

        return handleExceptionInternal(ex, apiErrorResponse, headers,
                HttpStatus.valueOf(apiErrorResponse.getStatusCode()), request);
    }

    @Override
    protected ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
                                                                  HttpHeaders headers,
                                                                  HttpStatus status,
                                                                  WebRequest request) {
        ApiErrorResponse apiErrorResponse = new ApiErrorResponse(status.value(), "Malformed JSON request");

        return handleExceptionInternal(ex, apiErrorResponse, headers,
                HttpStatus.valueOf(apiErrorResponse.getStatusCode()), request);
    }

}
