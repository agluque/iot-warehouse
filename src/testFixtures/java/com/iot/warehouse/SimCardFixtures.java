package com.iot.warehouse;

import com.iot.warehouse.core.SimCard;
import com.iot.warehouse.core.SimCardStatus;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import java.security.SecureRandom;
import java.util.UUID;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class SimCardFixtures {

    private static final SecureRandom random = new SecureRandom();

    public static SimCard activeSimCard() {
        return defaultSimCard().toBuilder()
                .status(SimCardStatus.ACTIVE)
                .build();
    }

    public static SimCard waitingForActivationSimCard() {
        return defaultSimCard().toBuilder()
                .status(SimCardStatus.WAITING_FOR_ACTIVATION)
                .build();
    }

    private static SimCard defaultSimCard() {
        return SimCard.builder()
                .id(random.nextLong())
                .simCardId(UUID.randomUUID())
                .country("Italy")
                .operatorCode("OC")
                .build();
    }

}
