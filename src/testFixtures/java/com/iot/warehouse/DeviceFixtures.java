package com.iot.warehouse;

import com.github.javafaker.Faker;
import com.iot.warehouse.core.Device;
import com.iot.warehouse.core.DeviceStatus;
import com.iot.warehouse.core.SimCard;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class DeviceFixtures {

    private static final Faker faker = new Faker();

    public static Device configuredDevice(SimCard simCard) {
        return defaultDevice(simCard).toBuilder()
                .temperature(faker.number().numberBetween(-25, 85))
                .status(DeviceStatus.READY)
                .build();
    }

    public static Device notReadyDevice(SimCard simCard) {
        return defaultDevice(simCard).toBuilder()
                .status(DeviceStatus.NOT_READY)
                .build();
    }

    private static Device defaultDevice(SimCard simCard) {
        return Device.builder()
                .id(faker.number().randomNumber())
                .name("DEVICE")
                .temperature(faker.number().numberBetween(-100, 100))
                .simCard(simCard)
                .build();
    }

}
