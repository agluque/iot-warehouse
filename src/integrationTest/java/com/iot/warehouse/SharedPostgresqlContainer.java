package com.iot.warehouse;

import org.testcontainers.containers.PostgreSQLContainer;

public class SharedPostgresqlContainer extends PostgreSQLContainer<SharedPostgresqlContainer> {

    private static final String POSTGRES_VERSION = "postgres:11.1";

    private static SharedPostgresqlContainer container;

    private SharedPostgresqlContainer() {
        super(POSTGRES_VERSION);
    }

    public static SharedPostgresqlContainer getInstance() {
        if (container == null) {
            container = new SharedPostgresqlContainer();
        }
        return container;
    }

    @Override
    public void start() {
        super.start();
    }

    @Override
    public void stop() {
    }

}
