package com.iot.warehouse;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.iot.warehouse.api.model.UpdateDeviceConfigurationRequest;
import com.iot.warehouse.core.Device;
import com.iot.warehouse.core.DeviceRepository;
import com.iot.warehouse.core.DeviceStatus;
import com.iot.warehouse.core.SimCardStatus;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.hamcrest.Matchers.hasSize;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
@AutoConfigureMockMvc
@Testcontainers
public class DeviceIntegrationTest {

    @Container
    private static final PostgreSQLContainer<SharedPostgresqlContainer> POSTGRES_CONTAINER = SharedPostgresqlContainer.getInstance();

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private DeviceRepository deviceRepository;

    @Autowired
    private ObjectMapper objectMapper;

    @BeforeEach
    public void setUp() {
        deviceRepository.deleteAll();
    }

    @Test
    void shouldRetrieveAllDevices() throws Exception {
        var waitingForActivationDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));
        var activeDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.activeSimCard()));

        mockMvc.perform(get("/api/warehouse/devices"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.devices", hasSize(2)))

                .andExpect(jsonPath("$.devices[0].id").value(waitingForActivationDevice.getId()))
                .andExpect(jsonPath("$.devices[0].temperature").value(waitingForActivationDevice.getTemperature()))
                .andExpect(jsonPath("$.devices[0].name").value(waitingForActivationDevice.getName()))
                .andExpect(jsonPath("$.devices[0].status").value(waitingForActivationDevice.getStatus().name()))
                .andExpect(jsonPath("$.devices[0].simCard.status").value(waitingForActivationDevice.getSimCard().getStatus().name()))
                .andExpect(jsonPath("$.devices[0].simCard.id").value(waitingForActivationDevice.getSimCard().getId()))
                .andExpect(jsonPath("$.devices[0].simCard.simCardId").value(waitingForActivationDevice.getSimCard().getSimCardId().toString()))
                .andExpect(jsonPath("$.devices[0].simCard.operatorCode").value(waitingForActivationDevice.getSimCard().getOperatorCode()))
                .andExpect(jsonPath("$.devices[0].simCard.country").value(waitingForActivationDevice.getSimCard().getCountry()))

                .andExpect(jsonPath("$.devices[1].id").value(activeDevice.getId()))
                .andExpect(jsonPath("$.devices[1].temperature").value(activeDevice.getTemperature()))
                .andExpect(jsonPath("$.devices[1].name").value(activeDevice.getName()))
                .andExpect(jsonPath("$.devices[1].status").value(activeDevice.getStatus().name()))
                .andExpect(jsonPath("$.devices[1].simCard.status").value(activeDevice.getSimCard().getStatus().name()))
                .andExpect(jsonPath("$.devices[1].simCard.id").value(activeDevice.getSimCard().getId()))
                .andExpect(jsonPath("$.devices[1].simCard.simCardId").value(activeDevice.getSimCard().getSimCardId().toString()))
                .andExpect(jsonPath("$.devices[1].simCard.operatorCode").value(activeDevice.getSimCard().getOperatorCode()))
                .andExpect(jsonPath("$.devices[1].simCard.country").value(activeDevice.getSimCard().getCountry()))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
        ;
    }

    @Test
    void shouldRetrieveAllDevicesWaitingForActivation() throws Exception {
        var waitingForActivationDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));
        deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.activeSimCard()));

        mockMvc.perform(get("/api/warehouse/devices")
                .param("simCardStatus", SimCardStatus.WAITING_FOR_ACTIVATION.name()))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.devices", hasSize(1)))
                .andExpect(jsonPath("$.devices[0].id").value(waitingForActivationDevice.getId()))
                .andExpect(jsonPath("$.devices[0].simCard.status").value(SimCardStatus.WAITING_FOR_ACTIVATION.name()))
        ;
    }

    @Test
    void shouldDeleteDevice() throws Exception {
        var existingDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));

        mockMvc.perform(delete("/api/warehouse/devices/{id}", existingDevice.getId()))
                .andExpect(status().isOk());

        Optional<Device> device = deviceRepository.findById(existingDevice.getId());

        assertThat(device).isEmpty();
    }

    @Test
    void shouldUpdateDeviceConfigurationStatus() throws Exception {
        var existingDevice = deviceRepository.saveAndFlush(DeviceFixtures.notReadyDevice(SimCardFixtures.waitingForActivationSimCard()));

        UpdateDeviceConfigurationRequest request = new UpdateDeviceConfigurationRequest(DeviceStatus.READY);

        mockMvc.perform(patch("/api/warehouse/devices/{id}", existingDevice.getId())
                .contentType(MediaType.APPLICATION_JSON)
                .content(objectMapper.writeValueAsString(request))).andExpect(status().isOk())
                .andExpect(jsonPath("$.id").value(existingDevice.getId()))
                .andExpect(jsonPath("$.temperature").value(existingDevice.getTemperature()))
                .andExpect(jsonPath("$.name").value(existingDevice.getName()))
                .andExpect(jsonPath("$.status").value(DeviceStatus.READY.name()))
                .andExpect(jsonPath("$.simCard.status").value(existingDevice.getSimCard().getStatus().name()))
                .andExpect(jsonPath("$.simCard.id").value(existingDevice.getSimCard().getId()))
                .andExpect(jsonPath("$.simCard.simCardId").value(existingDevice.getSimCard().getSimCardId().toString()))
                .andExpect(jsonPath("$.simCard.operatorCode").value(existingDevice.getSimCard().getOperatorCode()))
                .andExpect(jsonPath("$.simCard.country").value(existingDevice.getSimCard().getCountry()))
        ;

        Optional<Device> device = deviceRepository.findById(existingDevice.getId());

        assertThat(device).isPresent();
        assertThat(device.get().getStatus()).isEqualTo(DeviceStatus.READY);
    }

    @Test
    void shouldRetrieveOnSaleDevices() throws Exception {
        var configuredDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));
        deviceRepository.saveAndFlush(DeviceFixtures.notReadyDevice(SimCardFixtures.activeSimCard()));

        mockMvc.perform(get("/api/warehouse/devices/on-sale"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$.devices", hasSize(1)))
                .andExpect(jsonPath("$.devices[0].id").value(configuredDevice.getId()))
                .andExpect(jsonPath("$.devices[0].status").value(DeviceStatus.READY.name()))
        ;
    }

}
