package com.iot.warehouse.core;

import com.iot.warehouse.DeviceFixtures;
import com.iot.warehouse.SharedPostgresqlContainer;
import com.iot.warehouse.SimCardFixtures;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;
import org.springframework.data.domain.Pageable;
import org.testcontainers.containers.PostgreSQLContainer;
import org.testcontainers.junit.jupiter.Container;
import org.testcontainers.junit.jupiter.Testcontainers;

import static org.assertj.core.api.Assertions.assertThat;
import static org.springframework.boot.test.autoconfigure.jdbc.AutoConfigureTestDatabase.Replace.NONE;

@DataJpaTest
@AutoConfigureTestDatabase(replace = NONE)
@Testcontainers
class DeviceRepositoryTest {

    @Container
    private static final PostgreSQLContainer<SharedPostgresqlContainer> POSTGRES_CONTAINER = SharedPostgresqlContainer.getInstance();

    @Autowired
    private DeviceRepository deviceRepository;

    @BeforeEach
    public void setUp() {
        deviceRepository.deleteAll();
    }

    @Test
    void shouldFindAllDevicesBySimCardStatus() {
        var pageable = Pageable.ofSize(20);
        var waitingForActivationDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));
        deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.activeSimCard()));

        var devices = deviceRepository.findAllBySimCardStatus(SimCardStatus.WAITING_FOR_ACTIVATION, pageable);

        assertThat(devices).hasSize(1);
        assertThat(devices.getContent().get(0)).isEqualTo(waitingForActivationDevice);
    }

    @Test
    void shouldDeleteByIdNativeIfRowExists() {
        var waitingForActivationDevice = deviceRepository.saveAndFlush(DeviceFixtures.notReadyDevice(SimCardFixtures.waitingForActivationSimCard()));

        int modifiedRows = deviceRepository.deleteDeviceById(waitingForActivationDevice.getId());

        assertThat(modifiedRows).isEqualTo(1);
    }

    @Test
    void shouldNotDeleteByIdIfRowDoesNotExist() {
        var nonExistingDeviceId = 1L;

        int modifiedRows = deviceRepository.deleteDeviceById(nonExistingDeviceId);

        assertThat(modifiedRows).isEqualTo(0);
    }

    @Test
    void shouldReturnDevicesByStatusAndTemperatureBetween() {
        var configuredDevice = deviceRepository.saveAndFlush(DeviceFixtures.configuredDevice(SimCardFixtures.waitingForActivationSimCard()));
        deviceRepository.saveAndFlush(DeviceFixtures.notReadyDevice(SimCardFixtures.activeSimCard()));

        var pageable = Pageable.ofSize(20).withPage(0);

        var onSaleDevices = deviceRepository.findAllByStatusAndTemperatureBetween(DeviceStatus.READY,
                -25, 85, pageable);

        assertThat(onSaleDevices.getContent()).hasSize(1);
        assertThat(onSaleDevices.getContent().get(0)).isEqualTo(configuredDevice);
    }

}