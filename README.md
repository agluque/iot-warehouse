# IoT Warehouse

## Overview

Spring boot application exposing a simple REST API for managing IoT devices in a fictional warehouse.

## Development Tooling

- JDK 11
- Docker

## Tools/Libraries

- Spring Web
- Lombok
- OpenAPI
- Testcontainers

## Build

``` ./gradlew build ```

## Run

1. Run docker compose for running required resources:
``` docker-compose up -d ```

2. Run Spring Boot application with Demo profile in order to load a Catalogue of devices into database:
   ``` ./gradlew bootRunDemo ```

3. Alternative for running the application without loading data into database:
``` ./gradlew bootRun ```

Application will start by default in port 8080. Application API documentation can be found in the following URL:

``` http://localhost:8080/swagger-ui/index.html?configUrl=/api/docs/swagger-config ```

## Notes

Sorting and pagination are provided using built-in Spring Boot features. e.g:
``` http://localhost:8080/api/warehouse/devices?page=0&size=20&sort=status,asc ```